#encoding=utf-8
require "./utils/copy_data"
require "./install_config"

#set ssh port
set :port, $SSH_PORT

INSTALL_HOST.each do |host|
  role :install_host, host
end


task :copy, :role=>:install_host do
  #清理temp并重新创建
  run "rm -rf #{REMOTE_NGINX_TEMP_PATH}"
  run "mkdir -p #{REMOTE_NGINX_TEMP_PATH}"
  local_scp_to_hosts(INSTALL_HOST, "#{NGINX_LOCAL_PATH}/*", REMOTE_NGINX_TEMP_PATH);
end

task :pre_install, :role=>:install_host do
  #解压
  run "cd #{REMOTE_NGINX_TEMP_PATH}; tar -xzvf nginx.tar.gz;tar -xzvf zlib.tar.gz;tar -xzvf pcre.tar.gz;"
  #给每一个文件复制权限
  run "cd #{REMOTE_NGINX_TEMP_PATH}; chmod -R +x *"
end

task :install_nginx, :role=>:install_host do
  #./configure --with-zlib=/usr/local/zlib/ --with-pcre=/usr/local/pcre/ --prefix=/usr/local/nginx --with-http_stub_status_module
  run "cd #{REMOTE_NGINX_TEMP_PATH}/nginx;./configure --with-zlib=#{REMOTE_NGINX_TEMP_PATH}/zlib --with-pcre=#{REMOTE_NGINX_TEMP_PATH}/pcre --with-http_stub_status_module --prefix=#{NGINX_INSTALL_PATH} --with-http_ssl_module "
  run "cd #{REMOTE_NGINX_TEMP_PATH}/nginx;make&&make install"
end

task :update_path, :role=>:install_host do
  #修改/etc/bashrc文件
  run build_to_bashrc_str("#nginx config by ruby-deployer")
  run build_to_bashrc_str("NGINX_PATH=#{NGINX_INSTALL_PATH}")
  run build_to_bashrc_str("PATH=$NGINX_PATH/sbin:$PATH")
  run build_to_bashrc_str("export NGINX_PATH PATH")
end

task :update_nginx_with_bottle_config, :role=>:install_host do
  #back default nginx.conf
  run "cp #{NGINX_INSTALL_PATH}/conf/nginx.conf #{NGINX_INSTALL_PATH}/conf/nginx.conf.default_by_ruby_deploy -rf;"
  #copy bottle nginx.conf
  run "cp #{REMOTE_NGINX_TEMP_PATH}/nginx.conf #{NGINX_INSTALL_PATH}/conf/ -rf;"
  #reload nginx
  run "nginx"
end
#find_and_execute_task :install_zlib
  #find_and_execute_task :install_pcre
task :install, :role=>:install_host do
  find_and_execute_task :copy
  find_and_execute_task :pre_install
  find_and_execute_task :install_nginx
  find_and_execute_task :update_path
  find_and_execute_task :update_nginx_with_bottle_config
end
