#encoding=utf-8
################################################
#                                           全局配置部分
################################################
$SSH_PORT=22
################################################
#                                           数据配置部分
################################################
#远程数据服务器列表
DATA_REMOTE_HOST=["root@192.168.6.88"]

#远程数据服务器,操作数据路径,这个一定不是系统目录,不然clean会被破坏
DATA_REMOTE_TEMP_DIR="/home/QMMqAwQOuy.uucin.com/temp"

#远程服务器数据应该放置的目录
DATA_REMOTE_QQQ_PATH="/home/QMMqAwQOuy.uucin.com/earth"

#本地数据所在路径
DATA_LOCAL_PATH="/home/daigong/RubymineProjects/deploy_bottle/misc/bottle_deploy/data"
#本地数据所在路径中的压缩文件,暂时只支持tar.gz模式 tar -xzvf #{file_name} 自动加上后缀  .tar.gz
#约定压缩文件名字和压缩文件内名称一致
DATA_LOCAL_FILE=["workspaces"]
################################################
#                                           war配置部分
################################################
#Tomcat远程服务器列表
WAR_REMOTE_HOST=["root@192.168.6.29", "root@192.168.6.30", "root@192.168.6.31"]

#本地WAR路径
WAR_LOCAL_PATH="/home/daigong/RubymineProjects/deploy_bottle/misc/bottle_deploy/war"

#本地war名称,不需要带.war 自动加上的
WAR_LOCAL_FILE=["bottle", "bottle_app_samsung"]

#远程Tomcat路径
WAR_REMOTE_PATH="/usr/local/tomcat"
#操作过程中的temp路径
WAR_REMOTE_TEMP_PATH="#{WAR_REMOTE_PATH}/war_temp"
#WAR_REMOTE_LAST_BACKUP_PATH --->备份上次的war和解包文件
WAR_REMOTE_LAST_BACKUP_PATH="#{WAR_REMOTE_PATH}/last"
#war包应该放置的webapps路径
WAR_REMOTE_WEBAPPS_PATH="#{WAR_REMOTE_PATH}/webapps"
