#encoding=utf-8
require "./utils/copy_data"

$SSH_PORT=34529

set :port,$SSH_PORT

DATABASES_HOST= ["root@223.4.120.163","root@223.4.120.164","root@223.4.233.217","root@223.4.120.159"]

DATABASES_NAME= "20121107"

DATABASES_HOST.each { |host|
    role :datebase ,host
}

SEARCH_EEYE_DATA_FILE_PATH = "/home/daigong";

SEARCH_EEYE_DATA_FILE_NAME = "search_eeye_20121129.backup";

TEMP_PATH="/data/datebase_eeye"

def run_sql sql
    command = "psql -U postgres -d #{DATABASES_NAME} -c \"#{sql}\""
    run command;
end

task :deploy,:role=>:datebase do
    #清理原temp路径文件
    run "rm -rf #{TEMP_PATH}/*"
    run "mkdir -p #{TEMP_PATH}"
    #數據傳輸
    local_scp_to_hosts(DATABASES_HOST,"#{SEARCH_EEYE_DATA_FILE_PATH}/#{SEARCH_EEYE_DATA_FILE_NAME}",TEMP_PATH);
    puts "原數據數量"
    run_sql "select count(*) from geo.search_eeye"
    run_sql "DELETE FROM geo.search_eeye"
    puts "刪除後應該爲:0"
    run_sql "select count(*) from geo.search_eeye"
    run "pg_restore -U postgres -d #{DATABASES_NAME} -a -v \"#{TEMP_PATH}/#{SEARCH_EEYE_DATA_FILE_NAME}\" "
    puts "導入後爲非 0"
    run_sql "select count(*) from geo.search_eeye"
    puts "All Done >_<........."
end
