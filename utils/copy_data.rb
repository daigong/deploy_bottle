#encoding=utf-8

def local_scp_to(data_path, remote_host, remote_path, options={})
  port = 22;
  unless $SSH_PORT.nil?
    port = $SSH_PORT
  end
  unless options[:port].nil?
    port = options[:port]
  end
  command ="scp -P #{port}  -r #{data_path}  #{remote_host}:#{remote_path} "
  puts "begin #{command}"
  system command
  puts "end #{command}"
end

def local_scp_to_hosts(host_list, data_path, remote_path, options={})
  host_list.each do |remote_host|
    local_scp_to(data_path, remote_host, remote_path, options={})
  end
end

def build_to_bashrc_str(str)
  "echo '#{str} ' >> /etc/bashrc"
end
#将svn上的文件checkout到本地（而不是服务器上）
def svn_export svn_path,local_path

    for_check_out_path = svn_choose_path(svn_path)

    split_temp = for_check_out_path.split("/");

    export_name = split_temp[split_temp.length-1]

    puts "export #{for_check_out_path} to #{local_path}"

    `rm -rfv #{local_path}/#{export_name}`

    `svn export "#{for_check_out_path}" "#{local_path}/#{export_name}"`
end

#检查上一次执行的shell是否正常退出
def last_shell_ok?
    return $?==0
end
#判断一个字符串是否是str
def is_num?(str)
    Integer(str)
    rescue ArgumentError
        false
    else
        true
end
#返回svn路径svn_path下的list,返回类新 [true or false ,[path list]]
def svn_ls svn_path
    svn_shell = "svn ls #{svn_path}"

    ls_paths = `#{svn_shell}`

    if last_shell_ok?
        return true,ls_paths.split("\n")
    else
        puts "Shell: #{svn_shell} run wrong"
        return false,[]
    end
end

#base_path 字符串需要 “/结尾”代表参与选择交互，否则直接选择
def svn_choose_path(base_path="svn://192.168.6.5/suppose/System/tags/")
    if base_path.nil? or base_path.length==0
        raise ArgumentError,"base_path can'not be nil or empty String !!"
    end

    choose_path = base_path
    loop {
        if choose_path[choose_path.length-1] != "/"
            #选择路径不是目录，直接选择
            return choose_path
        end
        puts "选择svn路径，输入: $end 结束选择"
        puts "当前svn路径：#{choose_path}"
        success,path = svn_ls(choose_path)
        puts "请选择(No. or Name):"
        path.each_index do |index|
            puts "#{index}:#{path[index]}"
        end

        input_command = STDIN.gets

        if "$end" == input_command
            return choose_path
        else
            if is_num? input_command
                choose_path = choose_path << path[input_command.to_i]
            else
                if path.include?input_command
                    choose_path<<input_command
                else
                    puts "#{input_command} is not exist! Rechoose"
                end
            end
        end
    }
end

#sftp geto Date
def sftp_get hostname,user,remote_file,local_file
    Net::SFTP.start(hostname, user) do |sftp|
        sftp.download!( remote_file, local_file);
    end
end


