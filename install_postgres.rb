#encoding=utf-8
require "./utils/copy_data"
require "./install_config"

#set ssh port
set :port, $SSH_PORT

INSTALL_HOST.each do |host|
  role :install_host, host
end

################################################
#                                                 postgresql
################################################

task :copy, :role=>:install_host do
  #清理temp并重新创建
  run "rm -rf #{REMOTE_SQL_THINGS_TEMP_PATH}"
  run "mkdir -p #{REMOTE_SQL_THINGS_TEMP_PATH}"
  #准备数据
  local_scp_to_hosts(INSTALL_HOST, "#{SQL_THINGS_LOCAL_PATH}/*", REMOTE_SQL_THINGS_TEMP_PATH);
end

task :pre_install, :role=>:install_host do
  #解压
  run "cd #{REMOTE_SQL_THINGS_TEMP_PATH}; tar -xzvf geos.tar.gz;tar -xzvf postgis.tar.gz;tar -xzvf proj.tar.gz;"
  #给每一个文件复制权限
  run "cd #{REMOTE_SQL_THINGS_TEMP_PATH}; chmod -R +x *"
end

task :install_postgresql, :role=>:install_host do
  command="./postgresql.bin --mode unattended --prefix #{POSTGRESQL_INSTALL_PATH} --superpassword 123456 --locale zh_CN.utf8"
  puts "开始安装postgresql 可能过程有些长"
  #安装postgresql
  run "cd #{REMOTE_SQL_THINGS_TEMP_PATH};#{command}"
end

task :update_postgresql_password,:role=>:install_host do
  run "cp #{REMOTE_SQL_THINGS_TEMP_PATH}/pg_hba.conf #{POSTGRESQL_INSTALL_PATH}/data/ -rf"
  run "service postgresql-#{POSTGRESQL_VERSION} restart"
end

task :install_proj, :role=>:install_host do
  #./configure --prefix=/usr/local/proj
  run "cd #{REMOTE_SQL_THINGS_TEMP_PATH}/proj;./configure --prefix=#{PROJ_INSTALL_PATH}"
  run "cd #{REMOTE_SQL_THINGS_TEMP_PATH}/proj;make&&make install;"
end

task :install_geos, :role=>:install_host do
  #./configure --prefix=/usr/local/geos
  run "cd #{REMOTE_SQL_THINGS_TEMP_PATH}/geos;./configure --prefix=#{GEOS_INSTALL_PATH}"
  run "cd #{REMOTE_SQL_THINGS_TEMP_PATH}/geos;make&&make install;"
end

task :install_postgis, :role=>:install_host do
  #./configure --prefix=/usr/local/postgis
  # --with-pgsql=/usr/local/pgsql/bin/pg_config
  #--with-geos=/usr/local/geos/bin/geos-config
  #--with-proj=/usr/local/proj/
  configure_command = "./configure --prefix=#{POSTGIS_INSTALL_PATH} "+
      " --with-pgsql=#{POSTGRESQL_INSTALL_PATH}/bin/pg_config "+
      " --with-geos=#{GEOS_INSTALL_PATH}/bin/geos-config "+
      " --with-proj=#{PROJ_INSTALL_PATH}"
  run "cd #{REMOTE_SQL_THINGS_TEMP_PATH}/postgis;#{configure_command};"
  run "cd #{REMOTE_SQL_THINGS_TEMP_PATH}/postgis;make&&make install;"
end

task :install_postgis_patch, :role=>:install_host do
  run "createdb -U postgres postgis";
  run "psql -U postgres -d postgis -f #{POSTGIS_INSTALL_PATH}/share/lwpostgis.sql";
  run "psql -U postgres -d postgis -f #{POSTGIS_INSTALL_PATH}/share/lwpostgis_upgrade.sql";
  run "psql -U postgres -d postgis -f #{POSTGIS_INSTALL_PATH}/share/spatial_ref_sys.sql";
end

task :update_path, :role=>:install_host do
  #修改/etc/bashrc文件
  run build_to_bashrc_str("#postgresql config by ruby-deployer")
  run build_to_bashrc_str("PGSQL_HOME=#{POSTGRESQL_INSTALL_PATH}")
  run build_to_bashrc_str("PROJ_HOME=#{PROJ_INSTALL_PATH}")
  run build_to_bashrc_str("GEOS_HOME=#{GEOS_INSTALL_PATH}")
  run build_to_bashrc_str("POSTGIS_HOME=#{POSTGIS_INSTALL_PATH}")
  run build_to_bashrc_str("LD_LIBRARY_PATH=$PROJ_HOME/lib:$GEOS_HOME/lib:$POSTGIS_HOME/lib:$PGSQL_HOME/lib:$LD_LIBRARY_PATH")
  run build_to_bashrc_str("PATH=$PGSQL_HOME/bin:$PATH")
  run build_to_bashrc_str("export PGSQL_HOME PROJ_HOME GEOS_HOME POSTGIS_HOME LD_LIBRARY_PATH PATH")
end

task :update_ldconfig, :role=>:install_host do
  run "rm -rf /etc/ld.so.conf.d/bottle_postgres.conf"
  run "touch /etc/ld.so.conf.d/bottle_postgres.conf"
  run "echo '#{PROJ_INSTALL_PATH}/lib' >> /etc/ld.so.conf.d/bottle_postgres.conf"
  run "echo '#{GEOS_INSTALL_PATH}/lib' >> /etc/ld.so.conf.d/bottle_postgres.conf"
  run "echo '#{POSTGIS_INSTALL_PATH}/lib' >> /etc/ld.so.conf.d/bottle_postgres.conf"
  run "ldconfig"
end

task :install, :role=>:install_host do
  find_and_execute_task :copy
  find_and_execute_task :pre_install
  find_and_execute_task :install_postgresql
  find_and_execute_task :install_proj
  find_and_execute_task :install_geos
  find_and_execute_task :install_postgis
  find_and_execute_task :update_path
  find_and_execute_task :update_ldconfig
  find_and_execute_task :update_postgresql_password
  find_and_execute_task :install_postgis_patch
  puts "Job Done !!"
end

task :check_temp, :role=>:install_host do
  run "ls #{REMOTE_SQL_THINGS_TEMP_PATH}"
end
