#encoding=utf-8
require "./utils/copy_data"
require "./install_config"

#set ssh port
set :port, $SSH_PORT


INSTALL_HOST.each do |host|
  role :install_host, host
end
################################################
#                                                 JAVA
################################################
task :install_java, :role=>:install_host do
  #清理临时目录,并重新建立
  run "rm -rf #{JAVA_REMOTE_TEMP_PATH} -rf"
  run "mkdir -p #{JAVA_REMOTE_TEMP_PATH}"
  #拷贝安装文件到临时目录
  INSTALL_HOST.each do |host|
    local_scp_to("#{JAVA_LOCAL_PATH}/*", host, JAVA_REMOTE_TEMP_PATH, :port=>$SSH_PORT)
  end
  #给安装文件+x权限
  run "cd #{JAVA_REMOTE_TEMP_PATH};chmod +x #{JAVA_FILE_NAME} ;"
  #进入目录运行安装
  run "cd #{JAVA_REMOTE_TEMP_PATH};./#{JAVA_FILE_NAME} -mode=silent -silent_xml=./silent.xml"


end

task :update_path, :role=>:install_host do
  #修改/etc/bashrc文件
  run "echo '#java config  by ruby-deployer ' >> /etc/bashrc"
  run "echo 'JAVA_HOME=#{JAVA_INSTALL_PATH} ' >> /etc/bashrc"
  run "echo 'CLASS_PATH=$JAVA_HOME/lib:$JAVA_HOME/jre/lib ' >> /etc/bashrc"
  run "echo 'PATH=$JAVA_HOME/bin:$PATH' >> /etc/bashrc"
  run "echo 'export JAVA_HOME CLASS_PATH PATH' >>/etc/bashrc"
end

task :install, :role=>:install_host do
  find_and_execute_task :install_java
  find_and_execute_task :update_path
end


task :check_java, :role=>:install_host do
  run "ls #{JAVA_INSTALL_PATH};true"
end

task :drop_java, :role=>:install_host do
  run "rm -rf #{JAVA_INSTALL_PATH} "
end
