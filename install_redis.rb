#encoding=utf-8
require "./utils/copy_data"
require "./install_config"

INSTALL_HOST.each do |host|
  role :install_host, host
end
#set ssh port
set :port, $SSH_PORT


task :copy, :role=>:install_host do
  #清理temp并重新创建
  run "rm -rf #{REMOTE_REDIS_TEMP_PATH}"
  run "mkdir -p #{REMOTE_REDIS_TEMP_PATH}"
  local_scp_to_hosts(INSTALL_HOST, "#{REDIS_LOCAL_PATH}/*", REMOTE_REDIS_TEMP_PATH);
end

task :pre_install, :role=>:install_host do
  #解压
  run "cd #{REMOTE_REDIS_TEMP_PATH}; tar -xzvf redis.tar.gz"
  #给每一个文件复制权限
  run "cd #{REMOTE_REDIS_TEMP_PATH}/redis; chmod -R +x *"
end

task :install_redis, :role=>:install_host do
  run "cd #{REMOTE_REDIS_TEMP_PATH}/redis;make"
  run "cp #{REMOTE_REDIS_TEMP_PATH}/redis #{REDIS_INSTALL_PATH} -rf"
end

#拷貝Tomcat目录到远程
task :install, :role=>:tomcat_host do
  find_and_execute_task :copy
  find_and_execute_task :pre_install
  find_and_execute_task :install_redis
  puts "Redis install Job Done - w -"
end
