#encoding=utf-8
require "./utils/copy_data"
require "./install_config"

#set ssh port
set :port, $SSH_PORT

INSTALL_HOST.each do |host|
  role :install_host, host
end


task :copy_config, :role=>:install_host do
  local_scp_to_hosts(INSTALL_HOST, "#{COMMON_IPTABLES_CONFIG_LOCAL_PATH}/iptables", COMMON_IPTABLES_CONFIG_REMOTE_PATH);
end

task :service_restart, :role=>:install_host do
  run "/sbin/service iptables restart"
end


task :install, :role=>:install_host do
  find_and_execute_task :copy_config
  find_and_execute_task :service_restart
end
