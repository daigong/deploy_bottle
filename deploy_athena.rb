#encoding=utf-8
#
$SSH_PORT = 34529

require "./utils/copy_data"


#set ssh port
set :port, $SSH_PORT

#系统自己加.war后缀
ATHENA_WAR_NAME= "UUAthena"

ATHENA_LOCAL_WAR="/home/daigong/#{ATHENA_WAR_NAME}.war"

TEMP_PATH="/data/athena_temp"

TOMCAT_PATH="/data/tomcat7-athena"

TOMCAT_WEBAPPS_PATH="#{TOMCAT_PATH}/webapps"

TOMCAT_APP_BACKUP_PATH="#{TOMCAT_PATH}/last"

ATHENA_HOST=["root@223.4.21.21"]

ATHENA_HOST.each do |host|
  role :web,host
end


task :deploy, :role=>:web do
  #清理原temp路径文件
  run "rm -rf #{TEMP_PATH}/*"
  run "mkdir -p #{TEMP_PATH}"
  #上传
  ATHENA_HOST.each do |host|
    local_scp_to(ATHENA_LOCAL_WAR, host, TEMP_PATH)
  end
  #清理上次备份,备份原ROOT到TOMCAT_APP_BACKUP_PATH
  run "rm -rf #{TOMCAT_APP_BACKUP_PATH}"
  run "mkdir -p #{TOMCAT_APP_BACKUP_PATH}"
  run "cp -rf #{TOMCAT_WEBAPPS_PATH}/ROOT  #{TOMCAT_APP_BACKUP_PATH}/;true"
  #删除原war,并等待Tomcat自己删除
  run "rm -rf #{TOMCAT_WEBAPPS_PATH}/#{ATHENA_WAR_NAME}.war"
  puts "暂停一段时间,等待Tomcat自己删除 begin"
  sleep 30
  puts "暂停一段时间,等待Tomcat自己删除 end"
  #开始部署
  run "mv #{TEMP_PATH}/*.war #{TOMCAT_WEBAPPS_PATH}/"
  puts "暂停一段时间,等待Tomcat解包 begin"
  sleep 30
  puts "暂停一段时间,等待Tomcat解包 end"
  #开始部署ROOT
  run "rm -rf #{TOMCAT_WEBAPPS_PATH}/ROOT/"
  sleep 30
  run "cp -rf #{TOMCAT_WEBAPPS_PATH}/#{ATHENA_WAR_NAME}/ #{TOMCAT_WEBAPPS_PATH}/ROOT"
end
