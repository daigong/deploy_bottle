#encoding=utf-8
require "./utils/copy_data"
require "./install_config"

#set ssh port
set :port, $SSH_PORT

INSTALL_HOST.each do |host|
  role :install_host, host
end


task :install_snmp, :role=>:install_host do
  run "yum install net-snmp -y"
end

task :copy_config, :role=>:install_host do
  local_scp_to_hosts(INSTALL_HOST, "#{SNMP_LOCAL_PATH}/snmpd.conf", SNMP_CONFIG_YUM_REMOTE_PATH);
end

task :service_restart, :role=>:install_host do
  run "/sbin/service snmpd restart"
end

task :update_chkconfig, :role=>:install_host do
  run "/sbin/chkconfig snmpd on"
end

task :install, :role=>:install_host do
  find_and_execute_task :install_snmp
  find_and_execute_task :copy_config
  find_and_execute_task :update_chkconfig
  find_and_execute_task :service_restart
  puts "TODO自行配置防火墙"
  puts "
  #snmp
  -A INPUT -m state --state NEW -m tcp -p tcp -i eth0 --dport 161 -j ACCEPT
  -A INPUT -m state --state NEW -m udp -p udp -i eth0 --dport 161 -j ACCEPT
  "
end
