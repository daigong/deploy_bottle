#encoding=utf-8
require "./utils/copy_data"
require "./install_config"

#set ssh port
set :port, $SSH_PORT

INSTALL_HOST.each do |host|
  role :install_host, host
end

task :copy, :role=>:install_host do
  #清理temp并重新创建
  run "rm -rf #{REMOTE_TOMCAT_TEMP_PATH}"
  run "mkdir -p #{REMOTE_TOMCAT_TEMP_PATH}"
  local_scp_to_hosts(INSTALL_HOST, "#{TOMCAT_LOCAL_PATH}/*", REMOTE_TOMCAT_TEMP_PATH);
end

task :pre_install, :role=>:install_host do
  #解压
  run "cd #{REMOTE_TOMCAT_TEMP_PATH}; tar -xzvf tomcat.tar.gz"
  #给每一个文件复制权限
  run "cd #{REMOTE_TOMCAT_TEMP_PATH}/tomcat/bin; chmod -R +x *"
end

task :install_tomcat, :role=>:install_host do
  run "mv #{REMOTE_TOMCAT_TEMP_PATH}/tomcat #{TOMCAT_INSTALL_PATH}"
end

#拷貝Tomcat目录到远程
task :install, :role=>:tomcat_host do
  find_and_execute_task :copy
  find_and_execute_task :pre_install
  find_and_execute_task :install_tomcat
end
