#encoding=utf-8
require "./utils/copy_data"
require "./install_config"

#set ssh port
set :port, $SSH_PORT

INSTALL_HOST.each do |host|
  role :install_host, host
end


task :copy, :role=>:install_host do
  #清理temp并重新创建
  run "rm -rf #{REMOTE_NAGIOS_TEMP_PATH}"
  run "mkdir -p #{REMOTE_NAGIOS_TEMP_PATH}"
  local_scp_to_hosts(INSTALL_HOST, "#{NAGIOS_LOCAL_PATH}/*", REMOTE_NAGIOS_TEMP_PATH);
end

task :pre_install, :role=>:install_host do
  #解压
  run "cd #{REMOTE_NAGIOS_TEMP_PATH}; tar -xzvf nagios-plugins.tar.gz;tar -xzvf nrpe.tar.gz;"
  #给每一个文件复制权限
  run "cd #{REMOTE_NAGIOS_TEMP_PATH}; chmod -R +x *"
end

task :install_nagios_plugins, :role=>:install_host do
  run "cd #{REMOTE_NAGIOS_TEMP_PATH}/nagios-plugins/;./configure --prefix=#{NAGIOS_INSTALL_PATH}"
  run "cd #{REMOTE_NAGIOS_TEMP_PATH}/nagios-plugins/;make&&make install"
end

task :install_nrpe, :role=>:install_host do
  #./configure --prefix=/usr/local/nagios
  #--with-nrpe-user=root
  #--with-nrpe-group=root
  #--with-nagios-user=root
  #--with-nagios-group=root
  run "cd #{REMOTE_NAGIOS_TEMP_PATH}/nrpe/;./configure --prefix=#{NAGIOS_INSTALL_PATH} --with-nrpe-user=root --with-nrpe-group=root --with-nagios-user=root --with-nagios-group=root"
  run "cd #{REMOTE_NAGIOS_TEMP_PATH}/nrpe/;make all;make install-plugin;make install-daemon;make install-daemon-config"
end

task :update_nrpe_config, :role=>:install_host do
  run "rm -rf #{NAGIOS_INSTALL_PATH}/etc/nrpe.cfg";
  run "cp #{REMOTE_NAGIOS_TEMP_PATH}/nrpe.cfg #{NAGIOS_INSTALL_PATH}/etc/nrpe.cfg";
  #自启动
  run "echo '#config by deploy_bottle' >> /etc/rc.local"
  run "echo '#{NAGIOS_INSTALL_PATH}/bin/nrpe -c #{NAGIOS_INSTALL_PATH}/etc/nrpe.cfg -d' >> /etc/rc.local"
end
#find_and_execute_task :install_zlib
#find_and_execute_task :install_pcre
task :install, :role=>:install_host do
  find_and_execute_task :copy
  find_and_execute_task :pre_install
  find_and_execute_task :install_nagios_plugins
  find_and_execute_task :install_nrpe
  find_and_execute_task :update_nrpe_config
  puts "TODO自行配置nagios的防火墙"
  puts "
  #nrpe ssl
  -A INPUT -m state --state NEW -m tcp -p tcp -i eth0 --dport 5666 -j ACCEPT
  -A INPUT -m state --state NEW -m tcp -p tcp -i eth0 --dport 445 -j ACCEPT
  "
end
