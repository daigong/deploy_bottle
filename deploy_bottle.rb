#encoding=utf-8
require "./utils/copy_data"
require "./deploy_bottle_config"


#set ssh port
set :port, $SSH_PORT

################################################
#                                           初始化部分
################################################

#init role data_host by DATA_HOST
DATA_REMOTE_HOST.each do |host|
  role :data_host, host
end
#init role tomcat_host by TOMCAT_HOST
WAR_REMOTE_HOST.each do |host|
  role :tomcat_host, host
end

################################################
#                                           复制数据部分
################################################

#拷贝数据到数据服务器,并且解压缩在temp目录中,等待移动
task :copy_data, :role=>:data_host do

  run "mkdir -p #{DATA_REMOTE_TEMP_DIR}"

  DATA_REMOTE_HOST.each do |host|
    DATA_LOCAL_FILE.each do |file_or_dir|
      local_scp_to("#{DATA_LOCAL_PATH}/#{file_or_dir}.tar.gz", host, DATA_REMOTE_TEMP_DIR);
    end
  end

  DATA_LOCAL_FILE.each do |file_or_dir|
    run "cd #{DATA_REMOTE_TEMP_DIR};tar -xzvf #{file_or_dir}.tar.gz";
  end

end

#拷贝之后移动文件到正确目录
task :mv_data, :role=>:data_host do

  #准备QQQ目录
  run "mkdir -p #{DATA_REMOTE_QQQ_PATH}"

  DATA_LOCAL_FILE.each do |file_or_dir|
    #每次都会备份上次的文件---> *.back ,删除上上次的文件
    run "rm -rf #{DATA_REMOTE_QQQ_PATH}/#{file_or_dir}.back"
    puts "备份上次的文件...... 无视出错 begin"
    #备份上次的文件,如果没有在终端可能会出现err提示,但是脚本还可以继续
    run "mv #{DATA_REMOTE_QQQ_PATH}/#{file_or_dir}  #{DATA_REMOTE_QQQ_PATH}/#{file_or_dir}.back;true"
    puts "备份上次的文件...... 无视出错 end"
    #移动新文件到正确的位置
    run "mv #{DATA_REMOTE_TEMP_DIR}/#{file_or_dir}  #{DATA_REMOTE_QQQ_PATH}/#{file_or_dir}"
  end
end

#清理Temp路径
task :clean_data, :role=>:data_host do
  run "rm -rf #{DATA_REMOTE_TEMP_DIR}/*"
end

#查看路径
task :ls_data, :role=>:data_host do
  run "ls #{DATA_REMOTE_QQQ_PATH};true"
end

task :data_main, :role=>:data_host do
  find_and_execute_task :clean_data
  find_and_execute_task :copy_data
  find_and_execute_task :mv_data
end

################################################
#                                           复制war包部分
################################################
task :copy_war, :role=>:tomcat_host do
  #清理WAR_REMOTE_TEMP_PATH路径,建立WAR_REMOTE_TEMP_PATH,上传新文件war到WAR_REMOTE_TEMP_PATH
  run "rm -rf #{WAR_REMOTE_TEMP_PATH}"
  run "mkdir -p #{WAR_REMOTE_TEMP_PATH}"
  WAR_REMOTE_HOST.each do |host|
    WAR_LOCAL_FILE.each do |war|
      local_scp_to("#{WAR_LOCAL_PATH}/#{war}.war", host, WAR_REMOTE_TEMP_PATH);
    end
  end
  #备份上次的war包到WAR_REMOTE_LAST_BACKUP_PATH
  WAR_LOCAL_FILE.each do |war|
    puts "以下操作请忽略錯誤,备份上次的war包到#{WAR_REMOTE_LAST_BACKUP_PATH} begin"
    run "cp -rf #{WAR_REMOTE_WEBAPPS_PATH}/#{war} #{WAR_REMOTE_LAST_BACKUP_PATH}/;true;"
    puts "以下操作请忽略錯誤,备份上次的war包到#{WAR_REMOTE_LAST_BACKUP_PATH} end"
  end
  #清理原来的war,将新的war替换到webapps中
  WAR_LOCAL_FILE.each do |war|
    run "rm -rf #{WAR_REMOTE_WEBAPPS_PATH}/#{war}  #{WAR_REMOTE_WEBAPPS_PATH}/#{war}.war;true;"
  end
  run "mv  #{WAR_REMOTE_TEMP_PATH}/* #{WAR_REMOTE_WEBAPPS_PATH}/"
  #清理WAR_REMOTE_TEMP_PATH路径
  run "rm  -rf #{WAR_REMOTE_TEMP_PATH}"
end

task :start_tomcat,:hosts=>"root@192.168.6.29" do
  run "cd #{WAR_REMOTE_PATH};bin/startup.sh"
end
################################################
#                                           工具部分
################################################
task :echo_path, :role=>[:data_host, :tomcat_host] do
  run "echo $PATH"
end

task :check_tomcat_webapps, :role=>:tomcat_host do
  #進入路徑,解压,删除tar包
  run "ls -al #{WAR_REMOTE_WEBAPPS_PATH}/"
end

task :check_tomcat_last, :role=>:tomcat_host do
  run "ls -al #{WAR_REMOTE_LAST_BACKUP_PATH}/"
end

task :drop_tomcat, :role=>:tomcat_host do
  run "rm -rf #{WAR_REMOTE_PATH}"
end

task :drop_tomcat_webapps, :role=>:tomcat_host do
  run "rm -rf #{WAR_REMOTE_WEBAPPS_PATH}/*"
end
#删除QQQ路径 慎用
task :drop_qqq, :role=>:data_host do
  run "rm -rf #{DATA_REMOTE_QQQ_PATH}"
end
#pkill -9 java
task :pkill_java ,:role=>:tomcat_host do
  run "pkill -9 java;true"
end
