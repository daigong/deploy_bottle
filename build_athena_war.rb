#encoding=utf-8
require "./utils/copy_data"

WAR_PATH="/home/daigong"

WAR_NAME="UUAthena.war"

SVN_START_PATH="svn://192.168.6.5/suppose/System/tags/UU_Athena/"

CONFIG_18_PATH= Dir.pwd<<"/misc/deploy_athena_18"

CONFIG_UUCIN_PATH= Dir.pwd << "/misc/deploy_athena"

CONFIG_NAME="config.properties"

task :svn do
    #export
    svn_export SVN_START_PATH,WAR_PATH
end

task :smb do
    sftp_get "192.168.6.8","root","/home/share/daigong/deploy-UUAthena/UUAthena.war","#{WAR_PATH}/#{WAR_NAME}"
end

task :"18" do
    #update config
    `zip #{WAR_PATH}/#{WAR_NAME} -d #{CONFIG_NAME}`
    `cp -rvf #{CONFIG_18_PATH}/#{CONFIG_NAME} #{WAR_PATH}`
    `cd #{WAR_PATH};zip #{WAR_NAME} -u #{CONFIG_NAME}`
    `rm -rvf #{WAR_PATH}/#{CONFIG_NAME}`
end

task :uucin do
    #update config
    `zip #{WAR_PATH}/#{WAR_NAME} -d #{CONFIG_NAME}`
    `cp -rvf #{CONFIG_UUCIN_PATH}/#{CONFIG_NAME} #{WAR_PATH}`
    `cd #{WAR_PATH};zip #{WAR_NAME} -u #{CONFIG_NAME}`
    `rm -rvf #{WAR_PATH}/#{CONFIG_NAME}`
end
