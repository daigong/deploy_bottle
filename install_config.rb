$SSH_PORT=34529

#项目所在路径
DEPLOY_ROOT="/home/daigong/RubymineProjects"

#要安装软件的服务器(需要将ssh授权配置好)
INSTALL_HOST=["root@223.4.233.196"]

REMOTE_TEMP_PATH="/data/src/"

#软件要安装在远程服务器的哪个路径
INSTALL_ROOT="/data/new-nginx/"
################################################
#                                                 JAVA
################################################

JAVA_LOCAL_PATH="#{DEPLOY_ROOT}/deploy_bottle/misc/java"
JAVA_FILE_NAME="jrrt-3.1.2-1.6.0-linux-x64.bin"
JAVA_REMOTE_TEMP_PATH="#{REMOTE_TEMP_PATH}/java"
#java安装目录,这个不能指定服务器的安装,
#服务器的安装目录请修改 misc/java/silent.xml文件中的USER_INSTALL_DIR属性
JAVA_INSTALL_PATH="#{INSTALL_ROOT}/jdk6"


################################################
#                                                 postgresql
################################################
SQL_THINGS_LOCAL_PATH="#{DEPLOY_ROOT}/deploy_bottle/misc/postgresql"

REMOTE_SQL_THINGS_TEMP_PATH="#{REMOTE_TEMP_PATH}/postgresql"

POSTGRESQL_VERSION=8.3

POSTGRESQL_INSTALL_PATH="#{INSTALL_ROOT}/pgsql"

PROJ_INSTALL_PATH="#{INSTALL_ROOT}/proj"

GEOS_INSTALL_PATH="#{INSTALL_ROOT}/geos"

POSTGIS_INSTALL_PATH="#{INSTALL_ROOT}/postgis"
################################################
#                                                 nginx
################################################
NGINX_LOCAL_PATH="#{DEPLOY_ROOT}/deploy_bottle/misc/nginx"

REMOTE_NGINX_TEMP_PATH="#{REMOTE_TEMP_PATH}/nginx"

NGINX_INSTALL_PATH="#{INSTALL_ROOT}/nginx"


################################################
#                                           Tomcat
################################################
#省略tar.gz 默认为内容与文件名一致
TOMCAT_LOCAL_PATH ="#{DEPLOY_ROOT}/deploy_bottle/misc/tomcat"

REMOTE_TOMCAT_TEMP_PATH="#{REMOTE_TEMP_PATH}/tomcat"
#遠程要拷貝到的路徑
TOMCAT_INSTALL_PATH="#{INSTALL_ROOT}/tomcat7"

################################################
#                                           Redis
################################################
#省略tar.gz 默认为内容与文件名一致
REDIS_LOCAL_PATH ="#{DEPLOY_ROOT}/deploy_bottle/misc/redis"

REMOTE_REDIS_TEMP_PATH="#{REMOTE_TEMP_PATH}/redis"
#遠程要拷貝到的路徑
REDIS_INSTALL_PATH="#{INSTALL_ROOT}/redis"
################################################
#                                           nagios_plugins
################################################
NAGIOS_LOCAL_PATH="#{DEPLOY_ROOT}/deploy_bottle/misc/nagios_plugins"

REMOTE_NAGIOS_TEMP_PATH="#{REMOTE_TEMP_PATH}/nagios_plugins"

NAGIOS_INSTALL_PATH="#{INSTALL_ROOT}/nagios"
################################################
#                                           snmp
################################################
#安装snmp config的位置
SNMP_CONFIG_YUM_REMOTE_PATH="/etc/snmp"

SNMP_LOCAL_PATH="#{DEPLOY_ROOT}/deploy_bottle/misc/snmp"
################################################
#                                           common iptables config
################################################
COMMON_IPTABLES_CONFIG_LOCAL_PATH="#{DEPLOY_ROOT}/deploy_bottle/misc/common_iptables_config"

COMMON_IPTABLES_CONFIG_REMOTE_PATH="/etc/sysconfig/"

################################################
#                                                 PYTHON
################################################
REMOTE_PYTHON_TEMP_PATH="#{REMOTE_TEMP_PATH}/python"

PYTHON_LOCAL_PATH="#{DEPLOY_ROOT}/deploy_bottle/misc/python"

PYTHON_INSTALL_PATH="#{INSTALL_ROOT}/python27"

PYTHON_EASY_INSTALL_EGG_PATH="#{REMOTE_PYTHON_TEMP_PATH}/setuptools-0.6c11-py2.7.egg"
