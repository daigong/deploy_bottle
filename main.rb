require 'rubygems'
require 'bundler/setup'

Bundler.require(:default)

require 'capistrano/cli'

commands = [];

commands[0] = %W(-f install_redis install)


commands.each { |command|
    puts "#{command} begin:......."
    Capistrano::CLI.parse(command).execute!
    puts "#{command} end.........."
}
