#encoding=utf-8
require "./utils/copy_data"
require "./install_config"

#set ssh port
set :port, $SSH_PORT


INSTALL_HOST.each do |host|
  role :install_host, host
end
################################################
#                                                 PYTHON
################################################
task :copy, :role=>:install_host do
  #清理temp并重新创建
  run "rm -rf #{REMOTE_PYTHON_TEMP_PATH}"
  run "mkdir -p #{REMOTE_PYTHON_TEMP_PATH}"
  #准备数据
  local_scp_to_hosts(INSTALL_HOST, "#{PYTHON_LOCAL_PATH}/*", REMOTE_PYTHON_TEMP_PATH);
end

task :pre_install, :role=>:install_host do
  #解压
  run "cd #{REMOTE_PYTHON_TEMP_PATH}; tar -xzvf python.tar.gz"
  #给每一个文件复制权限
  run "cd #{REMOTE_PYTHON_TEMP_PATH}; chmod -R +x *"
end

task :install_python, :role=>:install_host do
  run "cd #{REMOTE_PYTHON_TEMP_PATH}/python;./configure --prefix=#{PYTHON_INSTALL_PATH}"
  run "cd #{REMOTE_PYTHON_TEMP_PATH}/python;make&&make install"
end

task :update_path, :role=>:install_host do
  run build_to_bashrc_str("#python config by ruby-deployer")
  run build_to_bashrc_str("PATH=#{PYTHON_INSTALL_PATH}/bin:$PATH")
  run build_to_bashrc_str("export PATH")
end

task :install_easy_install_egg,:role=>:install_host do
  run "chmod +x #{PYTHON_EASY_INSTALL_EGG_PATH}"
  run "#{PYTHON_EASY_INSTALL_EGG_PATH}"
end


task :install, :role=>:install_host do
  find_and_execute_task :copy
  find_and_execute_task :pre_install
  find_and_execute_task :install_python
  find_and_execute_task :update_path
  find_and_execute_task :install_easy_install_egg
end

###########################################
#yum install zlib-devel openssl-devel -y
###########################################
