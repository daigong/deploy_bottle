require 'rubygems'
require 'bundler/setup'

Bundler.require(:default)

require 'capistrano/cli'

commands = [];

commands <<  %W(-f install_java.rb install)

commands << %W(-f install_postgres.rb install)

commands << %W(-f install_redis.rb install)

commands << %W(-f install_nginx.rb install)

commands<< %W(-f install_python.rb install)

commands<< %W(-f install_tomcat.rb install)


commands.each { |command|
    puts "#{command} begin:......."
    Capistrano::CLI.parse(command).execute!
    puts "#{command} end.........."
}
