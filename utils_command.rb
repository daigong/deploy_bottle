#encoding=utf-8
require "./utils/copy_data"

#set ssh port
set :port, 22

COMMAND_HOST=["root@218.24.94.221"]

COMMAND_HOST.each do |host|
  role :command_host, host
end

#quit 退出
task :command, :role=>:command_host do
  #清理temp并重新创建
  loop {
    input_command = STDIN.gets
    break if input_command == "quit\n"
    #去掉 \n
    command=input_command[0...input_command.length-1]
    begin
      run command
    rescue Capistrano::Error=>e
      puts "命令出現异常: #{e}";
    end
  }
end
